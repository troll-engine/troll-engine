// create new container
const cliProgress = require('cli-progress');

const multibar = new cliProgress.MultiBar({
    format: '{filename}\t' + chalk.green('{bar}') + ' {percentage}% | {value}/{total} MB | Speed: {speed}',
    clearOnComplete: false,
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
    hideCursor: true

});

module.exports = function (libraries) {
    consola.log('Updating: ' + libraries);
    let longest = 0;
    libraries.forEach((library) => {
        if (library.length > longest) longest = library.length;
    });

    let promises = [];
    libraries.forEach((library) => {
        promises.push(new Promise((resolve, reject) => {
            // Update libraries

            let tabs = 0;

            if (!Number.isInteger(library.length / 8)) {
                tabs = Math.abs(Math.ceil(library.length / 8));
            } else {
                tabs = (library.length / 8) + 1;
            }

            let longestTabs = 0;

            if (!Number.isInteger(longest / 8)) {
                longestTabs = Math.abs(Math.ceil(longest / 8));

            } else {
                longestTabs = (longest / 8) + 1;

            }

            for (let i = 1; i <= (longestTabs - tabs); i++) {
                library = library + '\t'
            }

            multibar.create(100, 25, {filename: library, speed: '25 mbps'})
            resolve();
        }));
    });

    Promise.all(promises).then(() => {
        multibar.stop();
    });
};
