// create new container
const cliProgress = require('cli-progress');


// {value}/{total} MB | Speed: {speed}
const multibar = new cliProgress.MultiBar({
    format: '{spinner}{filename}\t' + chalk.green('{bar}') + ' {percentage}% | {status}',
    clearOnComplete: false,
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
    hideCursor: true
});

module.exports = function (libraries) {
    consola.log('Installing: ' + libraries);
    let longest = 0;
    libraries.forEach((library) => {
        if (library.length > longest) longest = library.length;
    });

    let promises = [];
    libraries.forEach((library) => {
        promises.push(new Promise((resolve, reject) => {
            // Update libraries

            let tabs = 0;

            if (!Number.isInteger(library.length / 8)) {
                tabs = Math.abs(Math.ceil(library.length / 8));
            } else {
                tabs = (library.length / 8) + 1;
            }

            let longestTabs = 0;

            if (!Number.isInteger(longest / 8)) {
                longestTabs = Math.abs(Math.ceil(longest / 8));

            } else {
                longestTabs = (longest / 8) + 1;
            }

            for (let i = 1; i <= (longestTabs - tabs); i++) {
                library = library + '\t'
            }

            const installOra = ora('');
            const bar = multibar.create(100, 0, {filename: library, status: 'Downloading...', spinner: '  '});
            setTimeout(() => {
                installOra.render();
                bar.update(50, {spinner: installOra.frame(), status: '{value}/{total} MB | Speed: {speed}'})
                setTimeout(() => {
                    installOra.render();
                    bar.update(100, {spinner: installOra.frame(), status: 'Finished!'})
                    resolve();
                }, Math.random() * (8000 - 1000) + 1000)
            }, Math.random() * (8000 - 1000) + 1000)
        }));
    });

    Promise.all(promises).then(() => {
        multibar.stop();
    });
};
