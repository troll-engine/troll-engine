const prepare = require('./prepare');
const assets = require('./assets');
const copy = require('./copy');
const generate = require('./generate');
const patch = require('./patch');

module.exports = function () {
    prepare()
        .then(() => {
            consola.success('Prepare successful!');
        })
        .then(assets)
        .then(() => {
            consola.success('Assets successful!');
        })
        .then(copy)
        .then(() => {
            consola.success('Copy successful!');
        })
        .then(generate)
        .then(() => {
            consola.success('Generate successful!');
        })
        .then(patch)
        .then(() => {
            consola.success('Patch successful!');
        })
        .catch((error) => {
            consola.error(error);
        })
};
