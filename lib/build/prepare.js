const fs = require('fs');
const path = require('path');

module.exports = function (opts) {
    const promise = new Promise((resolve, reject) => {
        const dirs = [
            path.join(process.cwd(), 'build'),
            path.join(process.cwd(), 'dist'),
            path.join(process.cwd(), '.troll/platforms/android'),
            path.join(process.cwd(), '.troll/platforms/browser'),
            path.join(process.cwd(), '.troll/platforms/ios'),
            path.join(process.cwd(), '.troll/platforms/linux'),
            path.join(process.cwd(), '.troll/platforms/mac'),
            path.join(process.cwd(), '.troll/platforms/windows')
        ];

        let promises = [];

        dirs.forEach((dir) => {
            if (!fs.existsSync(dir)) {
                promises.push(new Promise((res, rej) => {
                    consola.log('Creating directory: ' + dir);
                    fs.mkdir(dir, {recursive: true}, (err) => {
                        if (err) rej(err);
                        res();
                    });
                }));
            }
        });

        Promise.all(promises)
            .then((values) => {
                consola.log('Prepared!');
                resolve();
            })
            .catch((err) => {
                consola.error(err);
                reject(err);
            });
    });
    return promise;
};
