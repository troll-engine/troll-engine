#!/usr/bin/env node

// Libraries
global.prompts = require('prompts');
global.consola = require('consola');
global.chalk = require('chalk');
global.boxen = require('boxen');
global.ora = require('ora');
const boxenUtils = require('../utils/boxen.js');
const path = require('path');
const deepmerge = require('deepmerge');

const configDefaults = {
    name: 'Troll Project'
};
const projectConfig = require(path.join(process.cwd(), './troll.config.js'));

const trollConfig = deepmerge(configDefaults, projectConfig);

consola.log(trollConfig.name);

function prepare() {
    global.appRoot = path.resolve(__dirname, '../');
    if (program.debug) {
        consola.level = 5;
    } else {
        consola.level = 0;
    }
}

global.program = require('commander');

program.version('0.0.1', '-v, --version', 'Output the current version');

program
    .helpOption('-h, --help', 'Output usage information');

program
    .option('-d, --debug', 'Output extra debugging')

program
    .command('init')
    .option('-n, --name [name]', 'Name')
    .description('Initiates a new project')
    .action((opts) => {
        const commandInit = require('../lib/init');
        prepare();
        commandInit(opts);
    });

program
    .command('start [platform]')
    .description('Starts the project for either the current or the specified platform')
    .action((platform) => {
        const commandStart = require('../lib/start');
        prepare();
        commandStart();
    });

program
    .command('build [platform]')
    .description('Builds the project non-interactively')
    .action((platform) => {
        const commandBuild = require('../lib/build');
        prepare();
        commandBuild();
    });

program
    .command('install <library...>')
    .description('Installs a specified library to the project')
    .action((libraries) => {
        const commandInstall = require('../lib/install');
        prepare();
        commandInstall(libraries);
    });

program
    .command('rm <library>')
    .description('Removes a specified library from the project')
    .action((library) => {
        const commandRm = require('../lib/rm');
        prepare();
        commandRm();
    });

program
    .command('search [query]')
    .description('Searches the repository for libraries')
    .action((query) => {
        const commandSearch = require('../lib/search');
        prepare();
        commandSearch();
    });

program
    .command('update [libraries...]')
    .description('Updates either all libraries to their latest versions or just the specified ones')
    .action((libraries) => {
        const commandUpdate = require('../lib/update');
        prepare();
        commandUpdate(libraries);
    });

program.parse(process.argv);

const Handlebars = require('handlebars');

if (program.args.length < 1) {
    prepare();

    consola.success('Interactive prompt...');
    const questions = [
        {
            type: 'autocomplete',
            name: 'value',
            message: 'Pick your favorite actor',
            choices: [
                { title: 'Cage' },
                { title: 'Clooney', value: 'silver-fox' },
                { title: 'Gyllenhaal' },
                { title: 'Gibson' },
                { title: 'Grant' }
            ]
        },
        {
            type: 'multiselect',
            name: 'value',
            message: 'Pick colors',
            choices: [
                { title: 'Red', value: '#ff0000' },
                { title: 'Green', value: '#00ff00', disabled: true },
                { title: 'Blue', value: '#0000ff', selected: true }
            ],
            max: 2,
            hint: '- Space to select. Return to submit'
        },
        {
            type: 'select',
            name: 'value',
            message: 'Pick a color',
            choices: [
                { title: 'Red', description: 'This option has a description', value: '#ff0000' },
                { title: 'Green', value: '#00ff00', disabled: true },
                { title: 'Blue', value: '#0000ff' }
            ],
            initial: 1
        }
    ];

    (async () => {
        const response = await prompts(questions);

        // => response => { username, age, about }
    })();
}
