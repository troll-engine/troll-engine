const chalk = require('chalk');

module.exports = {
    borderStyles: {
        success: {
            topLeft: chalk.blue('┌'),
            topRight: chalk.blue('┐'),
            bottomLeft: chalk.blue('└'),
            bottomRight: chalk.blue('┘'),
            horizontal: chalk.blue('─'),
            vertical: chalk.blue('│')
        }
    }
};
